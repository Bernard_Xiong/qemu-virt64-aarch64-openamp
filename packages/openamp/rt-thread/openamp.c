/**
  ******************************************************************************
  * @file   openamp.c
  * @author  MCD Application Team
  * @brief  Code for openamp applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics. 
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the 
  * License. You may obtain a copy of the License at:
  *                       opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

#include "openamp.h"
#include "metal/sys.h"
#include "metal/device.h"

#define SHM_DEVICE_NAME   "shm"

#define DBG_TAG           "openamp"
#define DBG_LVL           DBG_INFO
#include <rtdbg.h>
#include <rthw.h>
#include <interrupt.h>

#define OPENAMP_SGI_INTNO   7

/* Globals */

static struct metal_io_region *shm_io;

static struct virtio_device vdev;
static struct rpmsg_virtio_device rvdev;
static struct virtio_vring_info rvrings[2] = {
	[0] = {
		.info.align = VRING_ALIGNMENT,
	},
	[1] = {
		.info.align = VRING_ALIGNMENT,
	},
};
static struct virtqueue *vq[2];


/*
 * 32 bit: -1 == 0xffffffff
 * 64 bit: -1 == 0xffffffffffffffff
 */
#define DEFAULT_PAGE_SHIFT (-1UL)
#define DEFAULT_PAGE_MASK  (-1UL)

static metal_phys_addr_t shm_physmap[] = { SHM_START_ADDR };
static struct metal_device shm_device = {
	.name = SHM_DEVICE_NAME,
	.bus = NULL,
	.num_regions = 1,
	{
		{
			.virt       = (void *) SHM_START_ADDR,
			.physmap    = shm_physmap,
			.size       = SHM_SIZE,
			.page_shift = DEFAULT_PAGE_SHIFT,
			.page_mask  = DEFAULT_PAGE_MASK,
			.mem_flags  = 0,
			.ops        = { NULL },
		},
	},
	.node = { NULL },
	.irq_num = 0,
	.irq_info = NULL
};

static unsigned char virtio_get_status(struct virtio_device *vdev)
{
  return HWREG8(VDEV_STATUS_ADDR);
}

static void virtio_set_status(struct virtio_device *vdev, unsigned char status)
{
  HWREG8(VDEV_STATUS_ADDR) = status;
}

static uint32_t virtio_get_features(struct virtio_device *vdev)
{
	return (1UL << (VIRTIO_RPMSG_F_NS));
}

static void virtio_set_features(struct virtio_device *vdev,
				uint32_t features)
{
}

static void virtio_notify(struct virtqueue *vq)
{
    /* call SGI to generate notify */
    LOG_I("openamp_notify ...");

    /* 给核0发软中断, 这里需要修改 */
    //rt_hw_ipi_send(OPENAMP_SGI_INTNO, 0x1);

}

const struct virtio_dispatch dispatch = {
	.get_status = virtio_get_status,
	.set_status = virtio_set_status,
	.get_features = virtio_get_features,
	.set_features = virtio_set_features,
	.notify = virtio_notify,
};

static int OPENAMP_shmem_init(int RPMsgRole)
{
  int status = 0;
  struct metal_device *device;
  struct metal_init_params metal_params = METAL_INIT_DEFAULTS;

  metal_init(&metal_params);

  status = metal_register_generic_device(&shm_device);
  if (status != 0) {
    return status;
  }

  status = metal_device_open("generic", SHM_DEVICE_NAME, &device);
  if (status != 0) {
    return status;
  }

  shm_io = metal_device_io_region(device, 0);
  if (shm_io == NULL) {
    return -1;
  }

  return 0;
}

static struct rt_semaphore openamp_sema;

static void openamp_sgi_isr_handler(int vector, void *param)
{
    /* signal the semaphore */
  rt_sem_release(&openamp_sema);
}

int OPENAMP_Init(int RPMsgRole, rpmsg_ns_bind_cb ns_bind_cb)
{
  int status = 0;

  /* 之前需要完成的初始化: 
   *  - ARM GICv3(qemu)的初始u化，只能初始化CPU_INTERFACE, 全局的DIST_INTERFACE由HOST
   *   完成
   *  - 可需要初始化MMU, 建立恒等映射， 共享内存部分： VDEV_START_ADDR开始，大小为VDEV_SIZE的部分需要设置为
   *    DEVICE MEM, 也即无cache
   */
  /* 初始化核间中断所需要的信号量, 可以进一步优化 */
  if (rt_sem_init(&openamp_sema, "openamp_int_sem", 0, RT_IPC_FLAG_FIFO) != RT_EOK)
  {
    return RT_ERROR;
  }
    
  /* 初始化核间中断 */
  rt_hw_interrupt_install(OPENAMP_SGI_INTNO, openamp_sgi_isr_handler, 0, "OPENAMP IPI_HANDLER");

  /* 使能中断OPENAMP_SGI_INTNO */

  /* 共享内存布局如下
   * VDEV_START_ADDR  -> VDEV_START_ADDR+ VDEV_STATUS_SIZE 用于设备状态交换
   * VDEV_START_ADDR+ VDEV_STATUS_SIZE - > VDEV_START_ADDR + VDEV_SIZE - 2 * VDEV_STATUS_SIZE
   *  用于共享内存，传输数据
   * VRING_TX_ADDRESS(倒数第二个VDEV_STATUS_SIZE): TX ring info
   * VRING_RX_ADDRESS(倒数第一个VDEV_STATUS_SIZE): RX ring info
   */

  /* Libmetal Initialization */
  status = OPENAMP_shmem_init(RPMsgRole);
  if(status)
  {
    return status;
  }

	/* Virtqueue setup */
	vq[0] = virtqueue_allocate(VRING_SIZE);
	if (!vq[0]) {
		LOG_E("virtqueue_allocate failed to alloc vq[0]");
		return -1;
	}

	vq[1] = virtqueue_allocate(VRING_SIZE);
	if (!vq[1]) {
		LOG_E("virtqueue_allocate failed to alloc vq[1]");
		return -1;
	}

	rvrings[0].io = shm_io;
	rvrings[0].info.vaddr = (void *)VRING_TX_ADDRESS;
	rvrings[0].info.num_descs = VRING_SIZE;
	rvrings[0].info.align = VRING_ALIGNMENT;
	rvrings[0].vq = vq[0];

	rvrings[1].io = shm_io;
	rvrings[1].info.vaddr = (void *)VRING_RX_ADDRESS;
	rvrings[1].info.num_descs = VRING_SIZE;
	rvrings[1].info.align = VRING_ALIGNMENT;
	rvrings[1].vq = vq[1];

	vdev.role = RPMsgRole;
	vdev.vrings_num = VRING_COUNT;
	vdev.func = &dispatch;
	vdev.vrings_info = &rvrings[0];

  /* as a remote, no name service, no need to init shm pool */
	status = rpmsg_init_vdev(&rvdev, &vdev, NULL, shm_io, NULL);

  return 0;
}

void OPENAMP_DeInit()
{
  rpmsg_deinit_vdev(&rvdev);

  metal_finish();
}

int OPENAMP_create_endpoint(struct rpmsg_endpoint *ept, const char *name,
                            uint32_t dest, rpmsg_ept_cb cb,
                            rpmsg_ns_unbind_cb unbind_cb)
{
  return rpmsg_create_ept(ept, &rvdev.rdev, name, RPMSG_ADDR_ANY, dest, cb,
		          unbind_cb);
}

void OPENAMP_check_for_message(void)
{
  /* 等待来自SGI的信号 */
  rt_sem_take(&openamp_sema, RT_WAITING_FOREVER);
  /* openamp的virtual queue处理, 在任务上下午调用endpoint cb */
  virtqueue_notification(vq[1]);
}
