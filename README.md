# 使用说明

这个是一份针对aarch64的临时openamp移植，包括了openamp移植的代码（不是最新版本），用到的RT-Thread代码等，都包括在这个仓库内。

## 编译说明

在编译rt-thread的时候（假设是Linux下），需要安装`scons`等软件：

```shell
pip install scons
pip install libncurses5-dev lib32z1
```

设置好需要使用的工具链路径，一般aarch64-none-elf的工具链都可以：

```shell
export RTT_EXEC_PATH=/opt/toolchains/gcc-arm-11.2-2022.02-x86_64-aarch64-none-elf/bin
```

运行scons命令:

```shell
CC build/kernel/src/scheduler.o
CC build/kernel/src/thread.o
CC build/kernel/src/timer.o
LINK rtthread.elf
aarch64-none-elf-objcopy -O binary rtthread.elf rtthread.bin
aarch64-none-elf-size rtthread.elf
   text    data     bss     dec     hex filename
 167700    6272  173605  347577   54db9 rtthread.elf
scons: done building targets.
```

## 运行说明

在仓库根目录下有一份运行qemu的脚本：`qemu-oe.sh`

```text
qemu-system-aarch64 -M virt,gic-version=3 -m 1G -cpu cortex-a57 -smp 4 -kernel rtthread.elf -nographic \
-device virtio-serial-device -chardev socket,host=127.0.0.1,port=4321,server=on,wait=on,telnet=on,id=console0 -device virtserialport,chardev=console0
```

注意在运行脚本的最后加了句

```text
-device virtio-serial-device -chardev socket,host=127.0.0.1,port=4321,server=on,wait=on,telnet=on,id=console0 -device virtserialport,chardev=console0
```

相当于开启了一个virtio虚拟的串口，而RT-Thread的shell输出就是通过这份虚拟串口进行输出。执行的时候需要在一个终端下执行`qemu-oe.sh`，同时在另外的一个终端通过telnet到本机的4321端口去打开shell：

```sh
./qemu-oe.sh
```

```sh
telnet localhost 4321
```

执行结果如下图：

![run](figures/rtthread_run.png)

`openamp`的初始化导出成一个shell命令，执行后等待openamp master端ready：

![openamp](figures/openamp.png)


## 其他说明

rt-thread的执行地址被设置成了 [0x7a000000](link.lds#L27)

```text
SECTIONS
{
    . = 0x7a000000;
    . = ALIGN(4096);
    .text :
    {
        KEEP(*(.text.entrypoint))       /* The entry point */
        *(.vectors)
        *(.text)                        /* remaining code */
        *(.text.*)                      /* remaining code */
...
```

也可以通过`scons --menuconfig`打开menuconfig开启早期的print

![earlyprint](figures/menuconfig.png)

这样它会通过实际的qemu串口打印输出信息，而不完全使用telnet的方式（但不包括输入）

如果希望把命令行输出到qemu串口，可以在menuconfig中把控制台设备名设置成uart0（如果是virtio串口，请使用vport0p1）

![uart](figures/uart.png)
