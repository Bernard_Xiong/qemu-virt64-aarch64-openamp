qemu-system-aarch64 -M virt,gic-version=3 -m 1G -cpu cortex-a57 -smp 4 -kernel rtthread.elf -nographic \
-device virtio-serial-device -chardev socket,host=127.0.0.1,port=4321,server=on,wait=on,telnet=on,id=console0 -device virtserialport,chardev=console0
