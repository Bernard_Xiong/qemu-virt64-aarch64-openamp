/**
  ******************************************************************************
  * @file    openamp_conf.h
  * @author  MCD Application Team
  * @brief   Configuration file for OpenAMP MW
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics. 
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the 
  * License. You may obtain a copy of the License at:
  *                       opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __OPENAMP_CONF__H__
#define __OPENAMP_CONF__H__

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/

 /* Includes ------------------------------------------------------------------*/

 /* ########################## Linux Master Selection ############################## */
 /**
  * @brief Due to Linux compatibility, it's important to distinguish if the MASTER is Linux or not.
  *        In that case, the LINUX_RPROC_MASTER define is required
  */

/** @addtogroup OPENAMP_MW
  * @{
  */

/** @defgroup OPENAMP_CONF OPENAMP_CONF
  * @brief Configuration file for Openamp mw
  * @{
  */

/** @defgroup OPENAMP_CONF_Exported_Defines OPENAMP_CONF_Exported_Defines
  * @brief Defines for configuration of the Openamp mw
  * @{
  */

#define VDEV_START_ADDR   (0x70000000)
#define VDEV_SIZE	        (0x30000)
#define VDEV_STATUS_ADDR	VDEV_START_ADDR
#define VDEV_STATUS_SIZE	(0x4000)

#define SHM_START_ADDR	(VDEV_START_ADDR + VDEV_STATUS_SIZE)
#define SHM_SIZE		    (VDEV_SIZE - VDEV_STATUS_SIZE)

#define VRING_RX_ADDRESS	(VDEV_START_ADDR + SHM_SIZE - VDEV_STATUS_SIZE)
#define VRING_TX_ADDRESS	(VDEV_START_ADDR + SHM_SIZE)
#define VRING_ALIGNMENT		4
#define VRING_SIZE		    16

/* Fixed parameter */
#define NUM_RESOURCE_ENTRIES 2
#define VRING_COUNT          2
#define VDEV_ID              0xFF
#define VRING0_ID            0              /* VRING0 ID (master to remote) fixed to 0 for linux compatibility*/
#define VRING1_ID            1              /* VRING1 ID (remote to master) fixed to 1 for linux compatibility  */

/**
  * @}
  */

/** @defgroup OPENAMP_CONF_Exported_Macros OPENAMP_CONF_Exported_Macros
  * @brief Aliases.
  * @{
  */

/* DEBUG macros */

#if defined (__LOG_TRACE_IO_) || defined(__LOG_UART_IO_)
  #define OPENAMP_log_dbg               log_dbg
  #define OPENAMP_log_info              log_info
  #define OPENAMP_log_warn              log_warn
  #define OPENAMP_log_err               log_err
#else
  #define OPENAMP_log_dbg(...)
  #define OPENAMP_log_info(...)
  #define OPENAMP_log_warn(...)
  #define OPENAMP_log_err(...)
#endif

/**
  * @}
  */

/** @defgroup OPENAMP_CONF_Exported_Types OPENAMP_CONF_Exported_Types
  * @brief Types.
  * @{
  */

/**
  * @}
  */

/** @defgroup OPENAMP_CONF_Exported_FunctionsPrototype OPENAMP_CONF_Exported_FunctionsPrototype
  * @brief Declaration of public functions for OpenAMP mw.
  * @{
  */

/* Exported functions -------------------------------------------------------*/

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

#ifdef __cplusplus
}
#endif

#endif /* __OPENAMP_CONF__H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
