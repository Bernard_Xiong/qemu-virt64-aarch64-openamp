
#ifndef RTCONFIG_PREINC_H__
#define RTCONFIG_PREINC_H__

/* Automatically generated file; DO NOT EDIT. */
/* RT-Thread pre-include file */

#define HAVE_CCONFIG_H
#define METAL_INTERNAL
#define OPENAMP_VERSION \"1.0.0\"
#define OPENAMP_VERSION_MAJOR 1
#define OPENAMP_VERSION_MINOR 0
#define OPENAMP_VERSION_PATCH 0
#define RT_USING_LIBC
#define RT_USING_NEWLIBC
#define _POSIX_C_SOURCE 1
#define __RTTHREAD__

#endif /*RTCONFIG_PREINC_H__*/
